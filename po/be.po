# Belarusian translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-08 16:37+0100\n"
"PO-Revision-Date: 2020-11-26 11:33+0000\n"
"Last-Translator: Zmicer <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian <https://translate.ubports.com/projects/ubports/"
"calculator-app/be/>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: ../app/engine/formula.js:176
msgid "NaN"
msgstr "Не лік"

#: ../app/ubuntu-calculator-app.qml:267
#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:1
msgid "Calculator"
msgstr "Калькулятар"

#: ../app/ubuntu-calculator-app.qml:271 ../app/ubuntu-calculator-app.qml:276
msgid "Favorite"
msgstr "Улюбёнае"

#: ../app/ubuntu-calculator-app.qml:323
msgid "Cancel"
msgstr "Скасаваць"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select All"
msgstr "Абраць усе"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select None"
msgstr "Нічога не абрана"

#: ../app/ubuntu-calculator-app.qml:340 ../app/ubuntu-calculator-app.qml:404
msgid "Copy"
msgstr "Скапіяваць"

#: ../app/ubuntu-calculator-app.qml:348 ../app/ubuntu-calculator-app.qml:450
msgid "Delete"
msgstr "Выдаліць"

#: ../app/ubuntu-calculator-app.qml:414
msgid "Edit"
msgstr "Рэдагаваць"

#: ../app/ubuntu-calculator-app.qml:428
msgid "Add to favorites"
msgstr "Дадаць ва ўлюбёнае"

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid
#. expressions
#: ../app/ubuntu-calculator-app.qml:566 ../app/ui/FavouritePage.qml:89
#: ../app/ui/Screen.qml:51
msgid "dd MMM yyyy"
msgstr "dd MMM yyyy"

#: ../app/ui/FavouritePage.qml:42
msgid "No favorites"
msgstr "Няма ўлюбёных"

#: ../app/ui/FavouritePage.qml:43
msgid ""
"Swipe calculations to the left\n"
"to mark as favorites"
msgstr ""
"Правядзіце па разліках улева,\n"
"каб дадаць іх ва ўлюбёныя"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: ../app/ui/LandscapeKeyboard.qml:37 ../app/ui/PortraitKeyboard.qml:60
msgid "log"
msgstr "журнал"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: ../app/ui/LandscapeKeyboard.qml:44 ../app/ui/PortraitKeyboard.qml:64
msgid "mod"
msgstr "модуль"

#: ../app/ui/Screen.qml:37
msgid "Just now"
msgstr "Толькі што"

#: ../app/ui/Screen.qml:39
msgid "Today "
msgstr "Сёння "

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#: ../app/ui/Screen.qml:43
msgid "hh:mm"
msgstr "hh:mm"

#: ../app/ui/Screen.qml:45
msgid "Yesterday"
msgstr "Учора"

#: ../app/ui/Walkthrough.qml:96
msgid "Skip"
msgstr "Мінуць"

#: ../app/welcomewizard/Slide1.qml:27
msgid "Welcome to Calculator"
msgstr "Вітаем у калькулятары"

#: ../app/welcomewizard/Slide1.qml:28
msgid "Enjoy the power of math by using Calculator"
msgstr "Адчуйце моц матэматыкі пры дапамозе калькулятара"

#: ../app/welcomewizard/Slide10.qml:27
msgid "Copy formula"
msgstr "Скапіяваць формулу"

#: ../app/welcomewizard/Slide10.qml:28
msgid "Long press to copy part or all of a formula to the clipboard"
msgstr ""
"Націсніце і патрымайце, каб скапіяваць частку або ўсю формулу ў буфер абмену"

#: ../app/welcomewizard/Slide11.qml:79
msgid "Enjoy"
msgstr "Карыстайцеся"

#: ../app/welcomewizard/Slide11.qml:88
msgid "We hope you enjoy using Calculator!"
msgstr "Мы спадзяемся, што вам спадабаецца калькулятар!"

#: ../app/welcomewizard/Slide11.qml:106
msgid "Finish"
msgstr "Скончыць"

#: ../app/welcomewizard/Slide2.qml:27
msgid "Scientific keyboard"
msgstr "Навуковая клавіятура"

#: ../app/welcomewizard/Slide2.qml:28
msgid "Access scientific functions with a left swipe on the numeric keypad"
msgstr ""
"Атрымайце доступ да навуковых функцый, правёўшы ўлева на лічбавай клавіятуры"

#: ../app/welcomewizard/Slide3.qml:27
msgid "Scientific View"
msgstr "Навуковы выгляд"

#: ../app/welcomewizard/Slide3.qml:28
msgid "Rotate device to show numeric and scientific functions together"
msgstr ""
"Павярніце тэлефон для таго, каб убачыць разам лічбавыя і навуковыя функцыі"

#: ../app/welcomewizard/Slide4.qml:27
msgid "Delete item from calculation history"
msgstr "Выдаліць элемент з гісторыі"

#: ../app/welcomewizard/Slide4.qml:28
msgid "Swipe right to delete items from calculator history"
msgstr "Правядзіце ўправа, каб выдаліць элементы з гісторыі калькулятара"

#: ../app/welcomewizard/Slide5.qml:27
msgid "Delete several items from calculation history"
msgstr "Выдаленне некалькіх элементаў з гісторыі вылічэнняў"

#: ../app/welcomewizard/Slide5.qml:28
msgid "Long press to select multiple calculations for deletion"
msgstr "Націсніце і патрымайце, каб абраць некалькі аперацый для выдалення"

#: ../app/welcomewizard/Slide6.qml:27
msgid "Delete whole formula at once"
msgstr "Выдаліць усю формулу"

#: ../app/welcomewizard/Slide6.qml:28
msgid "Long press '←' button to clear all formulas from input bar"
msgstr ""
"Націсніце і патрымайце кнопку \"←\", каб выдаліць усе формулы з панэлі ўводу"

#: ../app/welcomewizard/Slide7.qml:27
msgid "Edit item from calculation history"
msgstr "Змяніць элемент з гісторыі вылічэнняў"

#: ../app/welcomewizard/Slide7.qml:28
msgid "Swipe left and press pencil to edit calculation"
msgstr "Правядзіце ўлева і націсніце аловак, каб змяніць вылічэнне"

#: ../app/welcomewizard/Slide8.qml:27
msgid "Add new favourite"
msgstr "Дадаць ва ўлюбёнае"

#: ../app/welcomewizard/Slide8.qml:28
msgid "Swipe left and press star to add calculations to favourites view"
msgstr ""
"Правядзіце ўлева і націсніце кнопку зорачкі, каб дадаць вылічэнні ва ўлюбёнае"

#: ../app/welcomewizard/Slide9.qml:27
msgid "Edit formula"
msgstr "Рэдагаваць формулу"

#: ../app/welcomewizard/Slide9.qml:28
msgid "Click in the middle of a formula to edit in place"
msgstr "Націсніце ў сярэдзіне формулы, каб рэдагаваць яе"

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:2
msgid "A calculator for Ubuntu."
msgstr "Калькулятар для Ubuntu."

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:3
msgid "math;addition;subtraction;multiplication;division;"
msgstr "матэматыка;складанне;адніманне;памнажэнне;дзяленне;"
